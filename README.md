


#Чтобы создать машины с помощью terraform, необходимо:

1. Установить необходимое программное обеспечение, например:
sudo apt update
sudo apt install git -y
sudo apt install mc -y
sudo apt terraform -y

2. Склонировать код из репозитория:
git clone https://gitlab.com/ZRazif/gbproject.git

3. Перейти в папку скачанную из репозитория:
cd ./gbproject/terraform/vkcloud

4. Вносим данные в файл provider.tf:
mcedit provider.tf
а именно username и password

5. Проверяем синтаксисЖ
terraform validate

6. Генерирует файл изменений и показывает, что изменится при запуске, сейчас изменений быть не должно, так как ничего не изменяем:
terraform plan

7. Если необходимо задеплоить сервер с нуля, просто переименовываем например файл сервера APP01.tf на APP01.tf_

8. Убеждаемся что план соответствует тому что хотим сделать:
terraform plan

9. Смотрим изменения:
terraform apply
и пишем согласие yes - Enter

10. Заходим в ЛК, видим что инстанс удален

11. После этого обратное, APP01.tf_ переименовываем в APP01.tf

12. Генерирует файл изменений:
terraform plan

13. Смотрим изменения:
terraform apply
и пишем согласие yes - Enter

14. Если поднимались машины HAP01, MON01 или машина управления то в личном кабинете подцепляем им внешние IP



#Чтобы зайти на машину управления необходимо:

1. Дать необходимые права ключу, например так:
chown user:group /home/user/Загрузки/keypair-terraform.pem
chmod 600 /home/user/Загрузки/keypair-terraform.pem

2. Перекинуть его на машину управления, например так:
scp -i /home/user/Загрузки/keypair-terraform.pem /home/user/Загрузки/keypair-terraform.pem ubuntu@sys.zhookoff.net:/home/ubuntu/.ssh

3. Войти на машину, например так:
ssh -i /home/user/Загрузки/keypair-terraform.pem ubuntu@sys.zhookoff.net



#Чтобы зайти на прочие машины с машины управления необходимо:

1. Дать необходимые права ключу:
sudo chown ubuntu:ubuntu /home/ubuntu/.ssh/keypair-terraform.pem
sudo chmod 600 /home/ubuntu/.ssh/keypair-terraform.pem

2. Войти на прочие машины с машины управления:
HAP01:
ssh -i /home/ubuntu/.ssh/keypair-terraform.pem ubuntu@10.0.1.111
MON01:
ssh -i /home/ubuntu/.ssh/keypair-terraform.pem ubuntu@10.0.1.112
APP01:
ssh -i /home/ubuntu/.ssh/keypair-terraform.pem ubuntu@10.0.1.113
APP02:
ssh -i /home/ubuntu/.ssh/keypair-terraform.pem ubuntu@10.0.1.114
DB01:
ssh -i /home/ubuntu/.ssh/keypair-terraform.pem ubuntu@10.0.1.115



#Чтобы настроить прочие машины после их создания с помощью terraform, необходимо:

1. На машине управления установить необходимое пронраммное обеспечение:
sudo apt update
sudo apt install git -y
sudo apt install mc -y
sudo apt install ansible -y
sudo apt install sshpass -y

2. Создать папку, куда будет скачиваться код из репозитория:
mkdir /home/ubuntu/work

3. Перейти в созданную папку:
cd /home/ubuntu/work

4. Склонировать код из репозитория в созданную папку:
git clone https://gitlab.com/ZRazif/gbproject.git

5. Перейти в папку скачанную из репозитория:
cd gbproject
---------------------------------------------------
PS:
1. В последующем переходить в папку:
cd /home/ubuntu/work/gbproject

2. Скачивать возможные изменения из репозитория:
git pull

3. Переходить на нужную ветку репозитория:
git checkout man
---------------------------------------------------
 
6. Для настройки машин необходимо запускать скрипты из файла .gitlab-ci.yml

---------------------------------------------------
PS:
1. Переменные.

1.1. Переменные забиты в group_vars.
1.2. IP забиты в hosts.

2. .gitlab-ci.yml:

2.1. Задача initial_setup:
2.1.1. Скрипт ansible-playbook -i hosts -e "env_type=initial" initial_setup.yml (плейбук initial_setup.yml):
2.1.1.1. Роль timezone включает необходимую временную зону на машине.
2.1.1.2. Роль auxiliary_packages устанавливает дополнительные пакеты и обновляет все остальные на машине.

2.2. Задача db:
2.2.1. Скрипт ansible-playbook -i hosts -e "env_type=db_k" db.yml (плейбук db.yml):
2.2.1.1. Роль mariadb устанавливает базу данных mariadb.
2.2.1.2. Роль reboot перезапускает машину после установки mariadb.
2.2.1.3. Роль database создает базу данных и пользователя для wordpress.
2.2.1.4. Роль create_backup_folder создает папки, где будут храниться бекапы.
2.2.1.5. Роль transfer_database_files перекидывает бекап базы данных wordpress из репозитория на машину и заливает ее.
2.2.1.6. Роль tasks_in_crontab включает скрипт для ежедневного бекапирования.

2.3. Задача app:
2.3.1. Скрипт ansible-playbook -i hosts -e "env_type=app_k" app.yml (плейбук app.yml):
2.3.1.1. Роль nginx устанавливает nginx c с откорректированным конфигом.
2.3.1.2. Роль docker устанавливает docker.
2.3.1.3. Роль mariadb_client устанавливает клиент базы данных mariadb.
2.3.1.4. Роль reboot перезапускает машину после установки nginx и docker.
2.3.1.5. Роль create_backup_folder создает папки, где будут храниться бекапы.
2.3.1.6. Роль transfer_site_files перекидывает все файлы wordpress из репозитория на машину и docker-compose.yml для поднятия контейнера c wordpress.
2.3.1.7. Роль site_conf устанавливает конфиг nginx для сайта (http; закоменчено название сайта).
2.3.1.8. Роль container_launch инициализирует рой, сетку и запускает контейнер с wordpress.
2.3.1.9. Роль tasks_in_crontab включает скрипт для ежедневного бекапирования.

2.4. Задача hap:
2.4.1. Скрипт ansible-playbook -i hosts -e "env_type=hap_k" hap.yml (плейбук hap.yml):
2.4.1.1. Роль nginx устанавливает nginx c с откорректированным конфигом.
2.4.1.2. Роль reboot перезапускает машину после установки nginx и docker.
2.4.1.3. Роль balancer_conf устанавливает конфиг nginx для сайта (https; закоменчено название сайта; закоменчены блоки для https).

3. ВАЖНО!!!

Папка uploads появляется в папке wp-content только после загрузки какого либо файла через админку wordpress. После этого необходимо расскоментировать задачу для создания симлинка на папку uploads (roles --> site_conf --> tasks --> main.yml) и запустить ее отдельно.
---------------------------------------------------------
