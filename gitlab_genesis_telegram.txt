1. ОБращаемся к @BotFather
2. /newbot
3. Вводим имя бота = git_genesis
4. Указываем имя пользователя для бота (обязательно окончание _bot) = git_genesis_bot
5. Сохраняем выданный токен = 5837650013:AAFNBUaGlkZCeZGO07DDibgiERmS1ceB1sc
6. Создаем новый канал в Telegram = GitGenesis (id = -1001500170031)
7. Добавляем в этот канал нашего бота как администратора
8. Добавляем нас как подписчиков в данный канал
9. Узнаем имя канала (в данном варианте он как приватный): https://api.telegram.org/bot<token>/getUpdates (имя канала будет похоже, что-то вроде id: -10019749756699)
10. В GitLab в проекте, переходим: Setting -> Integrations -> Выбираем Telegram
11. Ставим галочку Active, в поле токена указываем токен бота, выделяем все Trigger, в поле Channel identifier указываем id канала
12. Test settings
