data "vkcs_compute_flavor" "APP02" { # Выбираем имя шаблона VM (предоставляемых VK Cloud)
  name = var.compute_flavor # Имя шаблона берется из переменной 
}

data "vkcs_images_image" "APP02" { # Выбираем имя образа VM (предоставляемых VK Cloud)
  name = var.image_flavor # Имя образа берется из переменной 
}

resource "vkcs_compute_instance" "APP02" { # Создаем VM (APP02)
  name                    = "APP02" # Имя сервера (берется из переменной)
  flavor_id               = data.vkcs_compute_flavor.APP02.id # Шаблон
  key_pair                = var.key_pair_name # Ключ SSH (настраивается в личном кабинете VK Cloud)
  security_groups         = ["default"] # Группы безопасности
  availability_zone       = var.availability_zone_name # Зона доступности

  block_device {
    uuid                  = data.vkcs_images_image.APP02.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
    fixed_ip_v4 = "10.0.1.114"
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnetwork
  ]
}