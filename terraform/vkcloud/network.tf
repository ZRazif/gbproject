# Создание внутренней сети
resource "vkcs_networking_network" "network" {
  name           = var.int_network_name # Определяем имя сети (берется из переменной)
  admin_state_up = "true" # Поднять (активировать) сеть
  description    = "Internal network" # Описание сети
}

# Создание подсети для внутренней сети
resource "vkcs_networking_subnet" "subnetwork" {
  name       = var.int_subnet_name # Определяем имя подсети (берется из переменной)
  network_id = vkcs_networking_network.network.id # Указываем в какой сети создать подсеть
  cidr       = var.int_subnet_cidr # Определяем CIDR для подсети (берется из переменной)
  allocation_pool { # Определяем pool (диапозон) адресов, выдаваемых DHCP-сервером
    start = "10.0.1.50"
    end   = "10.0.1.100"
  }
}

# Запрос данных для получения публичного IP (Floating IP)
data "vkcs_networking_network" "extnet" {
   name = "ext-net" # Внешние сети ext-net предоставляет VK Cloud
}

# Создание маршрутизатора для внешней сети и взаимодействия с внешним миром
resource "vkcs_networking_router" "router" {
   name                = var.ext_router_name
   admin_state_up      = true
   external_network_id = data.vkcs_networking_network.extnet.id
}

# Подключаем маршрутизатор к подсети (внутренней сети)
# Add SNAT and INTERFACE_DISTRIBUTED
resource "vkcs_networking_router_interface" "router_interface" {
   router_id = vkcs_networking_router.router.id # Определяем маршрутизатор
   subnet_id = vkcs_networking_subnet.subnetwork.id # Определяем необходимую подсеть
}

resource "vkcs_networking_secgroup" "secgroup_1" { # Создаем группу безопасности
   name = "admin" # Задаем имя группе безопасности
   description = "Terraform security group (admin)" # Задаем описание группе безопасности
}

resource "vkcs_networking_secgroup_rule" "secgroup_1_rule_1" { # Создаем правило в группе безопасности
   direction = "ingress" # Определяем входящий трафик
   port_range_max = 22 # 22/TCP (SSH)
   port_range_min = 22 # 22/TCP (SSH)
   protocol = "tcp" # Протокол TCP
   remote_ip_prefix = "0.0.0.0/0" # Все доступные сети
   security_group_id = vkcs_networking_secgroup.secgroup_1.id # Группа доступа
   description = "ssh" # Описание правила
}

resource "vkcs_networking_secgroup" "secgroup_2" { # Создаем группу безопасности
   name = "web" # Задаем имя группе безопасности
   description = "Terraform security group (web)" # Задаем описание группе безопасности
}

resource "vkcs_networking_secgroup_rule" "secgroup_2_rule_1" { # Создаем правило в группе безопасности
   direction = "ingress" # Определяем входящий трафик
   port_range_max = 443 # 443/TCP (HTTPS)
   port_range_min = 443 # 443/TCP (HTTPS)
   protocol = "tcp" # Протокол TCP
   remote_ip_prefix = "0.0.0.0/0" # Все доступные сети
   security_group_id = vkcs_networking_secgroup.secgroup_2.id # Группа доступа
   description = "https" # Описание правила
}