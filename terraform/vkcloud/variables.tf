# -------------------------------- #
#         Переменные сети          #
# -------------------------------- #

# Определение имени для внутренней сети
variable "int_network_name" {
  default = "int-net"
}

# Определение имени для внутренней подсети
variable "int_subnet_name" {
  default = "int-subnet"
}

# Определение CIDR для подсети
variable "int_subnet_cidr" {
  default = "10.0.1.0/24"
}

# Определение имени для маршрутизатора
variable "ext_router_name" {
  default = "ext_router"
}


# -------------------------------- #
#         Переменные VM            #
# -------------------------------- #

# image_flavor: имя образа виртуальной машины (openstack image list)
# compute_flavor: имя шаблона конфигурации виртуальной машин (openstack flavor list)
# key_pair_name: имя ключевой пары, которая будет использоваться для подключения к виртуальной машине по SSH
# availability_zone_name: имя зоны доступности, где будет размещена виртуальная машина

variable "image_flavor" {
  type = string
  default = "Ubuntu-20.04.1-202008"
}

variable "compute_flavor" {
  type = string
  default = "Basic-1-2-20"
}

variable "key_pair_name" {
  type = string
  default = "keypair-terraform"
}

variable "availability_zone_name" {
  type = string
  default = "MS1"
}